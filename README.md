# Arrays

Aquesta és una llibreria que conté algunes funcions de vector i matrius. Està pensada per utilitzar-la per fer pràctiques de depuració, i també per realitzar diferents tipus de test utilitzant el framework JUnit. 

<!-- 

La teoria per la depuració la podeu trobar [en aquest enllaç](https://www.ies-eugeni.cat/mod/resource/view.php?id=184279). 



I en [aquest altre enllaç](https://www.ies-eugeni.cat/mod/resource/view.php?id=96562) teniu la teoria per la part de tests JUnit.

-->



![arrays.png](arrays.png)





