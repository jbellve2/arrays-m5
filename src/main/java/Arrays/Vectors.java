package Arrays;


public class Vectors implements Globals{


    // <editor-fold defaultstate="collapsed" desc="MÈTODES VECTORS ...">

    /**
     * CREA UN VECTOR DE NÚMEROS ALEATORIES ENTRE 0 i max-1 I TAMANY max
     *
     * @param max màxim del vector i màxim número(-1) del vector
     * @return vector de números aleatoris entre 0 i max-1
     */
    public  int[] creaVector(int max) {
        int[] v = new int[max];
        int i;
        for (i = 0; i < max; i++) {
            v[i] = rnd.nextInt(max);
        }

        return v;
    }

    /**
     * imprimeix un vector
     *
     * @param vec vector a imprimir
     */
    public  void imprimeixVector(int vec[]) {
        int i;
        for (i = 0; i < vec.length; i++) {
            System.out.print(vec[i] + " ");
        }
    }

    /**
     * comprova parells
     *
     * @param num num a analitzar
     * @return retorna si el nombre és parell o senar
     */
    public  boolean comprovaParell(int num) {
        //tornarà 0 si és parell o 1 si és senar

        if (num % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Borra els parells d'un vector i retorna el vector sols amb els senars
     *
     * @param vec vector a analitzar
     * @return vector amb sols els senars
     */
    public  int[] borraParells(int vec[]) {
        int i;
        int num; //comprova si el número és parell

        for (i = 0; i < vec.length; i++) {
            num = vec[i];
            if (!comprovaParell(num)) {
                vec[i] = rnd.nextInt(vec.length);
                i--;
            }
        }
        return vec;
    }

    /**
     * Copia el contingut de 3 vectors passats per paràmetre a un altre vector
     * @param v1 vector 1 a copiar
     * @param v2 vector 2 a copiar
     * @param v3 vector 3 a copiar
     * @return Vectors amb la longitud dels 3 i el mateix contingut
     */
    public int[] mescla3Vectors(int v1[], int v2[], int v3[]) {
        int max = v1.length + v2.length + v3.length;
        int v[] = new int[max];
        int i;
        int cont = 0;

        for (i = 0; i < v1.length; i++) {
            v[cont] = v1[i];
            cont++;
        }
        for (i = 0; i < v2.length; i++) {
            v[cont] = v2[i];
            cont++;
        }
        for (i = 0; i < v3.length; i++) {
            v[cont] = v3[i];
            cont++;
        }


        return v;
    }


    /**
     * vector de números no repetits. entre 0 i max-1
     *
     * @param max màxim de números
     * @return vector de números no repetits entre 0 i max-1 de talla max
     */
     public int[] vectorNoRepetit(int max) {
        int[] v = new int[max];              //vector de int

        int i, j, num;
        //per a poder fer els numeros pseudoaleatoris

        int fallades = 0;
        //fallades: per a contar el nº de vegades
        //que troba un nº existent

        for (i = 0; i < max; i++) {

            //calcula un numero aleatori
            v[i] = rnd.nextInt(max);

            for (j = 0; j < i; j++) {
                if (v[i] == v[j]) {
                    fallades = fallades + 1;
                    i--;
                    j = max + 1;
                }
            }// final for i
        } // final for j

        return v;
    }

    /**
     * Passara el contingut d'un vector a una matriu
     *
     * @param vec      vector a convertir
     * @param files    num de files de la matriu
     * @param columnes num de columnes de la matriu
     * @return matriu de retorn
     */
     public int[][] vectorAMatriu(int vec[], int files, int columnes) {

        int maxVector = vec.length;
        int m[][] = new int[files][columnes];
        int i, j;
        int cont = 0;
        for (i = 0; i < files; i++) {
            for (j = 0; j < columnes; j++) {
                if (cont < maxVector)           // per si tinc mes files / columnes que quantitat de numeros en vector
                    m[i][j] = vec[cont];
                else
                    m[i][j] = 0;
                cont++;
            }
        }
        return m;
    }

    public int[] creaVectorSolsParells(int max) {
        int[] v = new int[max];
        for (int i = 0; i < max; i++) {
            v[i] = rnd.nextInt(max);
            if (!comprovaParell(v[i])) {
               i--;
            }
        }

        return v;
    }
    // </editor-fold>


}
