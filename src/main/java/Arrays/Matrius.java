package Arrays;

import pkgColors.Colors;

import java.util.Random;
import java.util.Scanner;

public class Matrius {

    static Scanner scan = new Scanner(System.in);
    static Random rnd = new Random();
    static Colors cl = new Colors();


    // <editor-fold defaultstate="collapsed" desc="FUNCIONS MATRIUS ...">
    //FUNCIONS
    public int[][] creaMatriu(int maxF, int maxC) {
       return creaMatriu(maxF,maxC,10);
    }

    public int[][] creaMatriu(int maxF,int maxC,int maxNum){
        int[][] mat = new int[maxF][maxC];
        int i, j;

        for (i = 0; i < maxF; i++) {
            for (j = 0; j < maxC; j++) {
                mat[i][j] = rnd.nextInt(maxNum);        // aleatori entre 0 i 9
            }
        }
        return mat;
    }

    public int[] matriuAVector(int m[][]) {
        int max = m.length * m[0].length;
        int v[] = new int[max];
        int i, j;
        int cont = 0;

        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[0].length; j++) {
                v[cont] = m[i][j];
                cont++;
            }
        }
        return v;
    }

    public void imprimeixMatriu(int mat[][]) {
        int i, j;
        for (i = 0; i < mat.length; i++) {
            for (j = 0; j < mat[i].length; j++) {
                if (mat[i][j] < 10)
                    System.out.print("   " + mat[i][j]);
                else if (mat[i][j] < 100)
                    System.out.print("  " + mat[i][j]);
                else
                    System.out.print(" " + mat[i][j]);
            }
            System.out.print("\n");
        }
    }

    public void imprimeixMatriuFormatSudoku(int mat[][]) {

        // IMPRIMIM EN FORMAT SUDOKU
        int i, j;
        System.out.print("\n\n\t\t  ** SUDOKU ** \n\n");

        for (i = 0; i < mat.length; i++) {
            if (i % 3 == 0) {
                System.out.print("\n\t\t");
            }
            for (j = 0; j < mat[i].length; j++) {
                if (j % 3 == 0) {
                    System.out.print("  ");
                }
                System.out.print(mat[i][j]);
            }
            System.out.print("\n\t\t");
        }
        System.out.print("\n\n");
    }

    public boolean buscaNum(int mat[][], int numero) {
        int i, j;
        for (i = 0; i < mat.length; i++) {
            for (j = 0; j < mat[i].length; j++) {
                if (mat[i][j] == numero) {
                    return true;
                }
            }
        }

//en cas que no hagem acabat abans
//vol dir que no existeix aquest número
//per tant tornem -1
        return false;
    }

    public int[][] matriuNoRepetits(int maxF, int maxC) {
          /*10) Programa per a crear una matriu de 3 x 3
        amb números entre 0 i 9 i que cap estiga repetit.
        Exemple:

        6 4 5
        3 9 8
        1 2 7

        */

        int totalNumeros = maxF * maxC;
        int v[];
        Vectors vec=new Vectors();
        int m[][] = new int[maxF][maxC];
        int cont;
        int i, j;

        // *******************************
        // VECTOR DE NÚMEROS NO REPETITS
        // *******************************
        // GUARDEM NUMEROS EN EL VECTOR
        // COMPROVANT QUE NO EXISTEIXEN JA
        // *******************************
        v = vec.vectorNoRepetit(totalNumeros);


        // *********
        // MATRIU //
        // *********
        //creem la matriu de numeros aleatoris

        cont = 0;         // contador del vector
        for (i = 0; i < maxF; i++) {
            for (j = 0; j < maxC; j++) {
                m[i][j] = v[cont] + 1;        // per fer-ho entre 1 i max (i així vàlid pel sudoku)
                cont++;
            }
        }

        return m;

    }

    public void imprimeixMatriuEnColorsAleatoris(int mat[][]) {
        String[] vecColors = {cl.roig, cl.cyan, cl.verd, cl.blau, cl.negre, cl.marro, cl.magenta, cl.gris, cl.nc};
        String colorAleatori;

        System.out.print("\t");
        int i, j;
        for (i = 0; i < mat.length; i++) {
            for (j = 0; j < mat[i].length; j++) {
               colorAleatori = vecColors[rnd.nextInt(vecColors.length)];
                System.out.print(colorAleatori + " " + mat[i][j] + cl.nc);
            }
            System.out.print("\n\t");
        }
    }


    // </editor-fold>


}
