package Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class VectorsTest {

    static Random rnd=new Random();
    static Vectors v=new Vectors();
    int max;

    @BeforeEach
    void setUp() {
        max=rnd.nextInt(20)+10; //entre 10 i 30
    }

    @Test
    void creaVector_Test() {
        int[] vec=v.creaVector(max);
        assertEquals(max,vec.length);
    }

    @Test
    void imprimeixVector_Test() {
        int[] vec=v.creaVector(max);
        v.imprimeixVector(vec);
    }

    @Test
    void comprovaParell_Test() {
        int[] vec=v.creaVectorSolsParells(max);
        v.imprimeixVector(vec);
        for (int i = 0; i <vec.length ; i++) {
            assertTrue(v.comprovaParell(vec[i]));
        }
    }


    @Test
    void borraParells_Test() {
        int[] vec=v.creaVector(max);
        v.imprimeixVector(vec);
        int[] vec2=v.borraParells(vec);
        v.imprimeixVector(vec2);
    }

    @Test
    void mescla3Vectors_Test() {
        int[] vec1=v.creaVector(max);
        int[] vec2=v.creaVector(max);
        int[] vec3=v.creaVector(max);
        v.imprimeixVector(vec1);
        v.imprimeixVector(vec2);
        v.imprimeixVector(vec3);
        int[] vec4=v.mescla3Vectors(vec1,vec2,vec3);
        v.imprimeixVector(vec4);
        assertEquals(max*3,vec4.length);
    }

    @Test
    void vectorNoRepetit_Test() {
        int[] vec=v.vectorNoRepetit(max);
        v.imprimeixVector(vec);
        assertEquals(max,vec.length);
    }

    @Test
    void vectorAMatriu_Test() {
        Matrius mt=new Matrius();
        int[] vec=v.creaVector(max);
        int[][] mat=v.vectorAMatriu(vec,vec.length/4,vec.length/4);
        mt.imprimeixMatriu(mat);
    }
}