package Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class MatriusTest {

    static Random rnd=new Random();
    static Matrius mt=new Matrius();
    int max;

    @BeforeEach
    void setUp() {
        max=rnd.nextInt(20)+10; //entre 10 i 30
    }

    @Test
    void creaMatriu_Test() {
        int[][]m=mt.creaMatriu(max,max);
        assertEquals(max,m.length);
    }

    @Test
    void matriuAVector_Test() {
        int[][]m=mt.creaMatriu(max,max);
        int[]v=mt.matriuAVector(m);
        assertEquals(max*max,v.length);
    }

    @Test
    void imprimeixMatriu_Test() {
        int[][]m=mt.creaMatriu(max,max);
        mt.imprimeixMatriu(m);
    }

    @Test
    void imprimeixMatriuFormatSudoku_Test() {
        System.out.println("matriu format sudoku");
        int[][]m=mt.creaMatriu(9,9);
        mt.imprimeixMatriuFormatSudoku(m);
    }

    @Test
    void buscaNum_Test() {
        int num= rnd.nextInt(9)+1;    //entre 1 i 9
        int[][]m=mt.creaMatriu(10,10);
        assertTrue(mt.buscaNum(m,num));
    }

    @Test
    void matriuNoRepetits_Test() {
        int[][]m=mt.matriuNoRepetits(10,12);
        mt.imprimeixMatriu(m);
        assertNotNull(m);
    }

    @Test
    void imprimeixMatriuEnColorsAleatoris_Test() {
        int[][]m=mt.creaMatriu(10,10);
        mt.imprimeixMatriuEnColorsAleatoris(m);
    }
}